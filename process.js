		let intervalloop = "";
		let foundEmails = [];
		let notFoundEmails = [];


		// handle enter key on interval input
		var onEnterInterval = document.getElementById("intervalID");
		onEnterInterval.addEventListener("keyup", function(event) {
		  if (event.keyCode === 13) {
		    event.preventDefault();
		    submitInterval();
		  }
		});

		// handle enter key on email input
		var onEnterEmail = document.getElementById("emailID");
		onEnterEmail.addEventListener("keyup", function(event) {
		  if (event.keyCode === 13) {
		    event.preventDefault();
		    checkEmail();
		  }
		});

		// INTERVAL SUBMIT AND VALIDATION
		function submitInterval(){

			let validateinput = document.getElementById("intervalID").value;
			// console.log(validateinput);
			// console.log(validateinput>0);
			// console.log(Number.isInteger(validateinput*1));
			if(validateinput>0 && Number.isInteger(parseInt(validateinput))){
				let inputdiv = document.getElementById("inputdiv").style.display;

				if(inputdiv=="none"){
					document.getElementById("inputdiv").style.display = "block";
				} else {
					document.getElementById("inputdiv").style.display = "none";
				}

				document.getElementById("intervaldiv").style.display = "none";

				texttowrite = ">>  Interval: " + validateinput + " seconds";
				writeTextResults(texttowrite);
				startShowingResults();

			} else {
					texttowrite = ">>  [ " + validateinput + " ]  is not valid. Please input number in seconds only. Thanks!";
					writeTextResults(texttowrite);
			}
		}

		// CHECKING INPUT FOR EMAILS AND COMMANDS
		function checkEmail(){

			let email = document.getElementById("emailID").value;
			document.getElementById("emailID").value = "";

		  	texttowrite = "  " + email + " ";
			writeTextResults(texttowrite);

			//check if program is running
			let isNotRunning = document.querySelector('#btnStop').disabled;
			if (isNotRunning==true){
				let isStart = email.toLowerCase();
				if(isStart=="start"){
					startShowingResults();
				} else if (isStart=="quit"){
					resetApp();
				} else {
					texttowrite = ">>  The program is not running, click start button or type start to execute program";
					writeTextResults(texttowrite);
				}
				return;
			} 

			// let email = document.getElementById("emailID").value;

		 //  	texttowrite = "  " + email + " ";
			// writeTextResults(texttowrite);

			let isEmail = ValidateEmail(email);

			if(isEmail == true){
				fetch('https://optizmotest.herokuapp.com/emaildata?email='+email+'')
				  .then((response) => {
				    return response.json();
				  })
				  .then((myJson) => {
				    // console.log(myJson);
				    if(myJson.length>0){
				    	//email found
						texttowrite = ">>  Email Address Found!";
						writeTextResults(texttowrite);
						foundEmails.push(email);
					} else {
						texttowrite = ">>  The email doesn't exist in the database!";
						writeTextResults(texttowrite);
						notFoundEmails.push(email);
					}
				});
			} else {
				let isCommand = email.toLowerCase();
				if(email=="start"){
					let status = document.querySelector('#btnStart').disabled;
					if (status==true){
						texttowrite = ">>  The program has already started";
						writeTextResults(texttowrite);
					} else {
						startShowingResults();
					}
				} else if (email=="stop") {
						stopShowingResult();
				} else if (email=="quit") {
						resetApp();
				} else {
					texttowrite = ">>  Invalid email or command";
					writeTextResults(texttowrite);
					texttowrite = ">>  Please enter valid email or command (start/stop/quit)";
					writeTextResults(texttowrite);
				}
			}
		}

		//START
		function startShowingResults(){

			texttowrite = ">>  STARTED";
			writeTextResults(texttowrite);

			let interval = document.getElementById("intervalID").value * 1000;
			intervalloop = setInterval(showResults, interval);
			document.querySelector('#btnStart').disabled = true;
			document.querySelector('#btnStop').disabled = false;

		}

		//STOP
		function stopShowingResult(){

			texttowrite = ">>  STOPPED";
			writeTextResults(texttowrite);

			clearInterval(intervalloop);
			document.querySelector('#btnStart').disabled = false;
			document.querySelector('#btnStop').disabled = true;

		}


		//QUIT
		function resetApp(){
			stopShowingResult();
			texttowrite = ">>  Thank you for using the program!";
			writeTextResults(texttowrite);
			setTimeout(function(){
				texttowrite = ">>  Quitting in 3...";
				writeTextResults(texttowrite);
			}, 2000);
			setTimeout(function(){
				texttowrite = ">>  Quitting in 2...";
				writeTextResults(texttowrite);
			}, 3000);
			setTimeout(function(){
				texttowrite = ">>  Quitting in 1...";
				writeTextResults(texttowrite);
			}, 4000);
			setTimeout(function(){
				texttowrite = ">>  Cya!";
				writeTextResults(texttowrite);
			}, 5000);
			setTimeout(function(){
				let intervalloop = "";
				let foundEmails = [];
				let notFoundEmails = [];

				document.getElementById("intervaldiv").style.display = "block";
				document.getElementById("inputdiv").style.display = "none";
				document.getElementById("intervalID").value = "";
				clearResults();
			}, 6000);
		}

		// CLEAR TEXT AREA
		function clearResults(){
			document.querySelector('#resultsID').value = "";
		}

		// WRITING TEXTS TO TEXT AREA
		function writeTextResults(texttowrite){
			let textarearesult = document.querySelector('#resultsID');
			let d = new Date();
				if(textarearesult.value==""){
					textarearesult.value = d.toLocaleTimeString() + " " + texttowrite;

				} else {
					textarearesult.value += " \\\n"+ d.toLocaleTimeString() + " " + texttowrite;
				}
				document.getElementById("resultsID").scrollTop = document.getElementById("resultsID").scrollHeight
		}

		// SHOWING EMAIL RESULTS
		function showResults()
		{	
			let overallResult = ">> ";

			if(foundEmails.length>0){
				let sortFound = foundEmails.sort();
				let noDupsFound = removeDuplicates(sortFound);
				noDupsFound.forEach(function(email) {
				  overallResult += " " + email + ": true,"
				});
			}
			if(notFoundEmails.length>0){
				let sortNotFound = notFoundEmails.sort();
				let noDupsNotFound = removeDuplicates(sortNotFound);
				noDupsNotFound.forEach(function(email) {
				  overallResult += " " + email + ": false,"
				});
			}


			if(foundEmails.length<1 && notFoundEmails.length<1)
			{
			  	texttowrite = ">> Please enter an email address to get started!";
				writeTextResults(texttowrite);
			} else {

			  	texttowrite = ">> Search results: ";
				writeTextResults(texttowrite);
				writeTextResults(overallResult);
			  	texttowrite = ">> Please enter the next email address!";
				writeTextResults(texttowrite);
			}
		}

		// REMOVE DUPLICATE EMAILS
		function removeDuplicates(array) {
		  return array.filter((a, b) => array.indexOf(a) === b)
		};
		
		// EMAIL VALIDATOR
		function ValidateEmail(inputText){
			var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
			if(inputText.match(mailformat))
			{
			return true;
			}
			else
			{
			return false;
			}
		}