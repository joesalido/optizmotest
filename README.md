Optizmo Developer Coding Test


Program created using Javascript, HTML and JSON as mock database.

The program accepts an X parameter which will serve as an interval for showing of results
Interval value can be inputted using 'Submit' button or by pressing 'Enter' key in the input box.
Interval input (X) has the following logical validations:
    1. user can't input any characters, numbers only.
    2. user can't input number that is equal or less than 0
    3. decimal value will be treated as a fraction of a second.

Once interval is set, the program will automatically start.
input will be accepted via the designated buttons or by pressing enter key on every input.
the program will run with the following validations/conditions:
    1. the user can only input valid email addresses. (e.g. "aasdsad" is invalid)
    2. the program will only record valid inputted emails.
    3. duplicate input of emails is allowed and will show if found or not, but it will only be displayed once in the final result.
    4. results were sorted with these conditions: The output must be sorted by the matched status then alphanumerically by email.
    5. the program accepts the ff commands with proper validation:
        - 'start' if the program is already running the program will notify. else, the program will continue on showing the serach results following the set interval.
        - 'stop' all inputs except start and quit will be accepted. start command will resume the processes and quit command will terminate the program. else the program will show error notication.
        - 'quit' the processes will be stopped and a termination message will appear then the program will reset.
        
Thank you for using the program!
